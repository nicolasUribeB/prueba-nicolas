import { Component, OnInit } from '@angular/core';
import { AppService } from '../../services/app.service';

declare let utils: any;

@Component({
  selector: 'app-opinion',
  templateUrl: './opinion.component.html',
  styleUrls: ['./opinion.component.scss'],
  providers: [AppService]
})
export class OpinionComponent implements OnInit {

  public infoOpiniones: Object;
  public sectionTitle: string;
  public arrEncuestas: Object[];
  

  constructor(private _appService: AppService) { }

  ngOnInit(): void {
    this.getData();
    utils.init();
    utils.countCharacter("idTextArea", 300);
  }

  getData() {
    this._appService.getDataOpinion().subscribe(
      result => {
        this.infoOpiniones = result.conocer_opinion;
        this.sectionTitle = this.infoOpiniones['title'];
        this.arrEncuestas = this.infoOpiniones['contenido'];
        localStorage.setItem('arrEncuestas', JSON.stringify(this.arrEncuestas));
      },
      err => console.log(err)
    );
  }

}
