import { Component, OnInit } from '@angular/core';
import { AppService } from '../../services/app.service';

@Component({
  selector: 'app-interes',
  templateUrl: './interes.component.html',
  styleUrls: ['./interes.component.scss'],
  providers: [AppService]
})
export class InteresComponent implements OnInit {

  public infoInteres: Object;
  public arrInteres: Object[];

  constructor(private _appService: AppService) { }

  ngOnInit(): void {
    this.getData();
  }

  getData() {
    this._appService.getDataIntereses().subscribe(
      result => {
        this.infoInteres = result;
        this.arrInteres = this.infoInteres['contenido'];
        localStorage.setItem('arrIntereses', JSON.stringify(this.arrInteres));
      },
      err => console.log(err)
    );
  }

}
