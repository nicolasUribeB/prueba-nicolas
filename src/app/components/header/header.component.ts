import { Component, OnInit, HostListener} from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  //Funcion creada de agregar o eliminar clase dependiendo el scroll en el top
  header_variable=false;
  @HostListener("document:scroll")
  scrollfunction(){
    if(document.body.scrollTop > 20 || document.documentElement.scrollTop > 20){
      this.header_variable=true;
    }
    else{
      this.header_variable=false;
    }
  }

}
