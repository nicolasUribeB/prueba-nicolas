import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()

export class AppService {
    public url: string;

    constructor(private http: HttpClient) {
        //declaracion de endPoint.
        this.url ="https://pruebanicolas-71e21.firebaseio.com/";
    }
    
    // metodos creados para consumir el json de interes y opinion

    getDataIntereses(): Observable<any> {
        return this.http.get(this.url+'interes.json',
            { headers: new HttpHeaders().set('Content-Type', 'application/json')    
        });
    }

    getDataOpinion(): Observable<any> {
        return this.http.get(this.url+'opinion.json',
            { headers: new HttpHeaders().set('Content-Type', 'application/json')    
        });
    }
}