# PruebaNicolas

Este proyecto fue generado con  [Angular CLI](https://github.com/angular/angular-cli) version 9.1.7.

Proyecto generado por medio de de los comandos de `ng new pruebaNicolas`

componentes generados por medio de los comandos `ng g c nombreComponente`


# Informacion General del proyecto

Se crea una carpeta de nombre "components" donde se almacena cada uno de los componentes generados que integran el proyecto "header, footer, interes, opinion".

se crea las hojas de estilos dentro de la carpeta styles, para cada uno de los componentes donde encontramos "variables, atoms, general, molecules, organisms, partials y main"

se crea una carpeta de servicios para consumir la BD almacenadas en firebase

Se utiliza Firebase para almacenar la base de datos (no relacional) y las imagenes.



## Servidor de desarrollo

Correr el proyecto con el comando `ng serve`. navegar en `http://localhost:4200/`. la aplicacion se recargara de forma automatica cuando se egenere algun cambio en los archivos.

